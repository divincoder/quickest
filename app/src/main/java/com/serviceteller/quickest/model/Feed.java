package com.serviceteller.quickest.model;

public class Feed {

    private String userName;
    private String postTime;
    private String feedBody;
    private String profilePhoto;
    private String feedImage;
    private int likeCount;
    private int commentCount;
    private String shareUrl;

    public Feed() {
    }

    public Feed(String userName, String postTime, String feedBody) {
        this.userName = userName;
        this.postTime = postTime;
        this.feedBody = feedBody;
//        this.likeCount = likeCount;
//        this.commentCount = commentCount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public String getFeedBody() {
        return feedBody;
    }

    public void setFeedBody(String feedBody) {
        this.feedBody = feedBody;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public String getFeedImage() {
        return feedImage;
    }

    public void setFeedImage(String feedImage) {
        this.feedImage = feedImage;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }
}
