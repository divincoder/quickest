package com.serviceteller.quickest;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;


public class QuickestApplication extends Application {

    private static QuickestApplication quickestApplication;

    public static QuickestApplication get(Context context) {
        return (QuickestApplication) context.getApplicationContext();
    }

    public static Resources getApplicationResources() {
        return getAppInstance().getResources();
    }

    public static QuickestApplication getAppInstance() {
        return quickestApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
