package com.serviceteller.quickest.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import com.serviceteller.quickest.QuickestApplication;


public abstract class BaseRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener, View.OnLongClickListener {
    public static Context context;
    LayoutInflater mInflater;
    private static Resources resources;


    public BaseRecyclerAdapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    public OnItemClickListener mItemClickListener;
    public ClickListener mClickListener;

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.mItemClickListener = itemClickListener;
    }

    public interface OnItemClickListener {
        public void onClick(int position, View view);
    }

    public interface OnToggleStateChangedListener {
        public void onToggleStateChanged(int position, CompoundButton compoundButton, boolean checked, boolean isUserInteraction);
    }

    public interface ClickListener {
        /**
         * Called when the view is clicked.
         *
         * @param v           view that is clicked
         * @param position    of the clicked item
         * @param isLongClick true if long click, false otherwise
         */
        public void onClick(View v, int position, boolean isLongClick);
    }

    /* Setter for listener. */
    public void setClickListener(ClickListener clickListener) {
        this.mClickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public boolean onLongClick(View v) {
        return true;
    }

    public static String getStringResource(int id) {
        if (resources == null) {
            resources = QuickestApplication.getApplicationResources();
        }
        return resources.getString(id);
    }

    public Resources getAppResources() {
        if (resources == null) {
            resources = QuickestApplication.getApplicationResources();
        }
        return resources;
    }
}
