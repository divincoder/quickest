package com.serviceteller.quickest.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.serviceteller.quickest.R;
import com.serviceteller.quickest.model.Feed;

import java.util.ArrayList;

public class FeedAdapter extends BaseRecyclerAdapter {

    private ClickListener clickListener;
    private ArrayList<Feed> feedList;

    public FeedAdapter(Context context, ClickListener clickListener) {
        super(context);
        this.clickListener = clickListener;
        this.feedList = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;
        RecyclerView.ViewHolder viewHolder = null;
        itemView = mInflater.inflate(R.layout.feed_item, parent, false);
        viewHolder = new ViewHolder(itemView, clickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Feed feed = feedList.get(position);
        final ViewHolder holder = (ViewHolder) viewHolder;
        holder.username.setText(feed.getUserName());
        holder.timeOfPost.setText(feed.getPostTime());
        holder.feedBody.setText(feed.getFeedBody());
    }

    @Override
    public int getItemCount() {
        return feedList == null ? 0 : feedList.size();
    }

    /*
        HELPER METHODS
     */

    public void setFeedList(ArrayList<Feed> feedList) {
        this.feedList = feedList;
        notifyDataSetChanged();
    }

    public void clearFeedList() {
        if (this.feedList != null) {
            feedList.clear();
            notifyDataSetChanged();
        }
    }

    public void addFeed(Feed feed) {
        feedList.add(feed);
        notifyItemInserted(feedList.size() - 1);
    }

    /*
            VIEW HOLDER
         */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView profileDp;
        TextView username;
        TextView timeOfPost;
        TextView feedBody;
        ImageView feedImage;
        TextView likesCount;
        TextView commentCount;
        TextView shareButton;

        private ClickListener clickListener;

        public ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            this.clickListener = clickListener;
            profileDp = itemView.findViewById(R.id.profile_dp);
            username = itemView.findViewById(R.id.userName);
            timeOfPost = itemView.findViewById(R.id.time_of_post);
            feedBody = itemView.findViewById(R.id.feed_body_text);
            feedImage = itemView.findViewById(R.id.feed_body_image);
            likesCount = itemView.findViewById(R.id.likes);
            commentCount = itemView.findViewById(R.id.comment);
            shareButton = itemView.findViewById(R.id.share_button);
        }
    }

}
