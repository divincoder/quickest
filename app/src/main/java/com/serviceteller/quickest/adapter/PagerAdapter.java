package com.serviceteller.quickest.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.serviceteller.quickest.fragment.AlertFragment;
import com.serviceteller.quickest.fragment.HotLinesFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ofoegbu Valentine on 27/04/2018.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {

    private List<String> mTabs = new ArrayList<>();

    public PagerAdapter(FragmentManager fm, List<String> mTabs) {
        super(fm);
        this.mTabs = mTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return AlertFragment.newInstance();
            case 1:
                return HotLinesFragment.newInstance();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabs.get(position);
    }
}
