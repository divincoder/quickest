package com.serviceteller.quickest.utils;

import android.support.v4.app.Fragment;
import android.view.View;

public class QuickestInterfaces {


//    public interface UserUpdateListener {
//        void onUserUpdated(User user);
//    }

    public interface ProfilePhotoUpdateListener {
        void onProfilePhotoUpdated(String encodedImage);
    }

    public interface FragmentOnBackPressedListener {
        void onBackPressed();
    }

    public interface FragmentSwitchedListener {
        void onFragmentChanged(Fragment fragment);
    }

    public interface UserAccountActivationListener {
        void onUserAccountActivated(boolean flag);
    }

    public interface AutoTopupStateListener {
        void onProfileChanged();
    }

    public interface PasswordDialogCallback {
        void onDismiss(boolean status, String message);
    }

//    public interface ProfileUpdateCallback {
//        void onDismiss(boolean status, User user, String message);
//    }


}
