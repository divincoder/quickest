package com.serviceteller.quickest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.github.ybq.android.spinkit.SpinKitView;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.serviceteller.quickest.R;
import com.serviceteller.quickest.fragment.EmergencyFragment;
import com.serviceteller.quickest.fragment.FeedFragment;
import com.serviceteller.quickest.fragment.LandingFragment;
import com.serviceteller.quickest.fragment.PlacesFragment;
import com.serviceteller.quickest.fragment.ProfileFragment;
import com.serviceteller.quickest.utils.QuickestInterfaces;

public class MainActivity extends BaseActivity implements EmergencyFragment.TabLayoutSetupCallback, QuickestInterfaces.FragmentSwitchedListener {

    private Toolbar toolbar;
    private TabLayout tabLayout;


    //Setup TabLayout
    @Override
    public void setupTabLayout(ViewPager viewPager) {

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(viewPager);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_feed:
                tabLayout.setVisibility(View.GONE);

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, FeedFragment.newInstance())
                        .commit();
                return true;
            case R.id.navigation_places:
                tabLayout.setVisibility(View.GONE);

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, PlacesFragment.newInstance())
                        .commit();

                return true;
            case R.id.navigation_emergency:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, EmergencyFragment.newInstance())
                        .commit();

                tabLayout.setVisibility(View.VISIBLE);
                return true;
            case R.id.navigation_profile:
                tabLayout.setVisibility(View.GONE);

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, ProfileFragment.newInstance())
                        .commit();
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.label_local_news));
        setSupportActionBar(toolbar);

        tabLayout = findViewById(R.id.tab_layout);

        //Initialize the Navigation drawer
        BottomNavigationViewEx navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navigation.enableAnimation(false);
        navigation.enableShiftingMode(false);
        navigation.enableItemShiftingMode(false);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, FeedFragment.newInstance())
                .commit();
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbarTitle(String title) {
        toolbar.setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_messages:
                intent = new Intent(MainActivity.this, MessageActivity.class);
                startActivity(intent);
                break;
            case R.id.action_notification:
                intent = new Intent(MainActivity.this, NotificationsActivity.class);
                startActivity(intent);
                break;
            case R.id.action_sign_out:
                break;
            case R.id.action_settings:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentChanged(Fragment fragment) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //LandingFragment.fragmentOnBackPressedListener.onBackPressed();
        startActivity(new Intent(MainActivity.this, AuthActivity.class));
    }
}