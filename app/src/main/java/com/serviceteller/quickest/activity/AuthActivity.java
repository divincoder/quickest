package com.serviceteller.quickest.activity;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;

import com.serviceteller.quickest.R;
import com.serviceteller.quickest.fragment.LandingFragment;
import com.serviceteller.quickest.utils.QuickestInterfaces;

public class AuthActivity extends BaseActivity implements QuickestInterfaces.FragmentSwitchedListener{
    Fragment frag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            frag = LandingFragment.newInstance();
//            if (viewType != null) {
//                switch (viewType) {
//                    case Constants.LOGIN_VIEW_TAG:
//                        frag = LoginFragment.newInstance(fragmentBundle);
//                        break;
//                    case Constants.REGISTRATION_VIEW_TAG:
//                        frag = SignupFragment.newInstance(fragmentBundle);
//                        break;
//                    case Constants.LANDING_VIEW_TAG:
//                        frag = LandingFragment.newInstance(fragmentBundle);
//                        break;
//                    default:
//                        frag = LandingFragment.newInstance(fragmentBundle);
//                        break;
//                }
//            } else {
//                frag = LandingFragment.newInstance(null);
            ft.replace(R.id.container, frag, frag.getClass().getSimpleName());
            ft.commit();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void navigateUp() {
        int backStack = getSupportFragmentManager().getBackStackEntryCount();
        if (backStack > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    @Override
    public void onFragmentChanged(Fragment fragment) {
        this.frag = fragment;
    }

}
