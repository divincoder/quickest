package com.serviceteller.quickest.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import com.serviceteller.quickest.R;

class BaseActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    public void switchFragment(Activity activity, Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = ((AppCompatActivity) activity).getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) {
            FragmentTransaction fragmentTransaction = manager.beginTransaction();
            ((MainActivity) this).onFragmentChanged(fragment);
            fragmentTransaction.replace(R.id.container, fragment, backStateName);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putSerializable(APP_SESSION, ApplicationSession.getInstance());
    }
}
