package com.serviceteller.quickest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.github.ybq.android.spinkit.SpinKitView;
import com.serviceteller.quickest.R;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private Button quickestLogo;
    private Button googleSignInButton;
    private Button phoneSignInButton;
    private SpinKitView spinKitView;
    private LinearLayout layoutView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

//        phoneSignInButton = findViewById(R.id.sign_in_with_phone_button);
//        phoneSignInButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


}
