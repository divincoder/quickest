package com.serviceteller.quickest.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.serviceteller.quickest.activity.MainActivity;
import com.serviceteller.quickest.R;
import com.serviceteller.quickest.activity.PostActivity;
import com.serviceteller.quickest.adapter.BaseRecyclerAdapter;
import com.serviceteller.quickest.adapter.FeedAdapter;
import com.serviceteller.quickest.model.Feed;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener {
    private ArrayList<Feed> feedList;
    private FeedAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private FloatingActionButton fab;

    public static FeedFragment newInstance() {
        FeedFragment fragment = new FeedFragment();

        return fragment;
    }

    public FeedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_feed, container, false);

        ((MainActivity) getActivity()).setToolbarTitle(getResources().getString(R.string.label_local_news));
        fab = rootView.findViewById(R.id.fab);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        feedList = new ArrayList<>();

        mAdapter = new FeedAdapter(getContext(), this);
        mRecyclerView = view.findViewById(R.id.feed_recycler);
        mAdapter.addFeed(new Feed("Ofoegbu Valentine", "2h ago", "An Old Man walking across the road just collapsed and " +
                "seem not breath well. Any doctor around who can help?"));
        mAdapter.addFeed(new Feed("Kenneth Ibeh", "5h ago", "Hey People!. I just had a great Local dish of Ayaraya Ji at Celebrities Restaurant. " +
                "I recommend you check them out whenever you are in Abakpa"));
        mAdapter.addFeed(new Feed("Chris Ikpeagha" ,"6h ago", "I just witnessed a major Accident along Ogui Road near Stadium. " +
                "Calling on nearby people to help move them to the Hospital."));
        mRecyclerView.setAdapter(mAdapter);

        fab.setOnClickListener(view1 -> {
            Intent intent = new Intent(getActivity(), PostActivity.class);
            startActivity(intent);
        });

    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {

    }
}
