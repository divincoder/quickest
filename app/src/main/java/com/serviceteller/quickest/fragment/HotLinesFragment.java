package com.serviceteller.quickest.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.serviceteller.quickest.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotLinesFragment extends Fragment {

    public static HotLinesFragment newInstance() {
        HotLinesFragment fragment = new HotLinesFragment();

        return fragment;
    }

    public HotLinesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hotlines, container, false);
    }

}
