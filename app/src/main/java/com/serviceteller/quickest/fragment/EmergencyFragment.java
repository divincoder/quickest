package com.serviceteller.quickest.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.serviceteller.quickest.activity.MainActivity;
import com.serviceteller.quickest.R;
import com.serviceteller.quickest.adapter.PagerAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmergencyFragment extends Fragment {

    private TabLayoutSetupCallback mToolbarSetupCallback;
    private ArrayList<String> mTabNamesList;


    public static EmergencyFragment newInstance(){
        EmergencyFragment fragment = new EmergencyFragment();

        return fragment;
    }

    public EmergencyFragment() {
        // Required empty public constructor
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof MainActivity) {
            mToolbarSetupCallback = (TabLayoutSetupCallback) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement TabLayoutSetupCallback");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTabNamesList = new ArrayList<>();
        mTabNamesList.add(getString(R.string.alerts));
        mTabNamesList.add(getString(R.string.hotlines));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_emergency, container, false);

        ((MainActivity) getActivity()).setToolbarTitle(getResources().getString(R.string.label_emergency));

        // Use PagerAdapter to manage page views in fragments.
        final PagerAdapter adapter = new PagerAdapter
                (getFragmentManager(),mTabNamesList );
        ViewPager viewPager = rootView.findViewById(R.id.view_pager);
        viewPager.setAdapter(adapter);

        mToolbarSetupCallback.setupTabLayout(viewPager);

        return rootView;
    }

    public interface TabLayoutSetupCallback {
        void setupTabLayout(ViewPager viewPager);
    }

}
