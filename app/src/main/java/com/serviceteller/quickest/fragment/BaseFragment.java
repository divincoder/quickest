package com.serviceteller.quickest.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.serviceteller.quickest.QuickestApplication;
import com.serviceteller.quickest.R;
import com.serviceteller.quickest.activity.AuthActivity;
import com.serviceteller.quickest.activity.ExitActivity;

public class BaseFragment extends Fragment {

    public void switchFragment(Fragment frag) {
        try {
            if (getActivity() != null && frag != null) {
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left, R.anim.pull_in_left, R.anim.push_out_right);
                fragmentTransaction.replace(R.id.container, frag, frag.getClass().getName());
                if (getActivity() instanceof AuthActivity) {
                    ((AuthActivity) getActivity()).onFragmentChanged(frag);
                }
                fragmentTransaction.addToBackStack(frag.getClass().getName());
                fragmentTransaction.commit();
            }
        } catch (Exception e) {
            ExitActivity.exitApplication(QuickestApplication.getAppInstance().getApplicationContext());
        }
    }

    public void switchFragmentWithoutBackStack(Fragment frag) {
        if (getActivity() != null && frag != null) {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left, R.anim.pull_in_left, R.anim.push_out_right);
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getSimpleName());
            fragmentTransaction.commit();
        }
    }
//    public void closeFragment() {
//        if (getActivity() != null) {
//            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
//                hideKeyboard();
//                if (AppLifecycleHandler.isApplicationVisible() || AppLifecycleHandler.isApplicationInForeground()) {
//                    getActivity().getSupportFragmentManager().popBackStack();
//                }
//            } else {
//                hideKeyboard();
//                getActivity().finish();
//            }
//        } else {
//            ExitActivity.exitApplication(AutoTopupApplication.getAppInstance().getApplicationContext());
//        }
//    }

    private void clearBackStack() {
        FragmentManager manager = getFragmentManager();
        FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
        manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

}
