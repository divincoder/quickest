package com.serviceteller.quickest.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.serviceteller.quickest.R;
import com.serviceteller.quickest.activity.AuthActivity;
import com.serviceteller.quickest.utils.QuickestInterfaces;

/**
 * A simple {@link Fragment} subclass.
 */
public class LandingFragment extends BaseFragment implements View.OnClickListener, QuickestInterfaces.FragmentOnBackPressedListener {

    Button loginButton;
    Button signUpButton;
    public static QuickestInterfaces.FragmentOnBackPressedListener fragmentOnBackPressedListener;

    public static LandingFragment newInstance() {
        LandingFragment frag = new LandingFragment();

        return frag;
    }

    public LandingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentOnBackPressedListener = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_landing, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        signUpButton = view.findViewById(R.id.sign_up_btn);
        loginButton = view.findViewById(R.id.login_btn);
        loginButton.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.sign_up_btn) {
            switchFragment(SignUpFragment.newInstance());

        } else if (v.getId() == R.id.login_btn) {
            switchFragment(LoginFragment.newInstance());
        }
    }

    @Override
    public void onBackPressed() {
        if (isAdded() && isVisible()) {
            getActivity().finish();
        } else {
            ((AuthActivity) getActivity()).navigateUp();
        }
    }
}
