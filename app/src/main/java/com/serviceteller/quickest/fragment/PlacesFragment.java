package com.serviceteller.quickest.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.serviceteller.quickest.activity.MainActivity;
import com.serviceteller.quickest.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlacesFragment extends Fragment {


    public static PlacesFragment newInstance(){
        PlacesFragment fragment = new PlacesFragment();

        return fragment;
    }

    public PlacesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_places, container, false);
        ((MainActivity) getActivity()).setToolbarTitle(getResources().getString(R.string.nearby_places));

        return rootView;
    }

}
